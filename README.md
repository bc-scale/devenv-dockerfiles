Development environments in a docker image 
==========================================

The idea is to provide development environments without the need to install 
nodejs on host computer. Build the docker image and run it in your project's 
root directory. It will open the `bash` terminal with nodejs installed. 
You can start developing your own nodejs project. 

Look into individual `*.Dockerfile` files for instructions and suggestions

Usage
=====

Build docker image for development, using some of available Dockerfiles, for example: 

    docker build --file devenv-node.Dockerfile --tag devenv-node:1.0 .

Start docker container for development: 

    docker run --rm -it -v `pwd`:/developer devenv-node:1.0

Inside this container, `/developer` directory will map directly to the 
root directory of the project, there you can do all command line commands 
to develop in given environment: compilations, packaging. 

You might probably need to map ports, depending on your specific user-case. For example, 
if you are developing react application, you might need to expose container's port 3000 in 
order to make the testing server visible from outside word.

Most probably you would need to run the IDE tools from your host computer, in order to 
modify files from your project's home directory. Don't know is there an elegant way 
to integrate your favorite IDE so that you can for example compile directly or run tests
from IDE. I understand it is great limitation if you are used to develop software
with an IDE. Fortunately, at least DeveloperStudio CODE with "remote" plugins can connect 
to a running development container, maybe also other IDEs have similar options. 
